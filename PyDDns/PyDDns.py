import os
import json
import logging
import argparse
import threading

import DHomeLogging
import PyTerminateHandler
import Monitor

MON_LIST = []

def LoadConfig(path):
	with open(path, 'r') as f:
		return json.loads(f.read())

def MonitorThread(**kwargs):
	mon = Monitor.Monitor(**kwargs)
	MON_LIST.append(mon)

	mon.ServeForever()

def main():
	parser = argparse.ArgumentParser(description='pyDDns')
	parser.add_argument('config', type=str, help='Path to the config file.')
	args = parser.parse_args()

	config = LoadConfig(args.config)

	### 1. Setup logging
	logFile = os.path.abspath(config['log'])
	logLvl = logging.__dict__[config['logLevel']]

	DHomeLogging.AddStderrOutput(level=logLvl)
	DHomeLogging.AddRotateFileOutput(filePath=logFile, level=logLvl)

	### 2. Setup Monitor
	mons = []
	for monSpec in config['monitors']:
		mons.append(
			threading.Thread(target=MonitorThread, kwargs=monSpec)
		)

	### 3. Init terminate handler
	PyTerminateHandler.Init()

	### 4. Start Monitors
	for mon in mons:
		mon.start()

	### 5. Wait for terminate signal
	PyTerminateHandler.TERMINATION_EVENT.wait()

	### 6. Exit process
	for mon in MON_LIST:
		mon.Shutdown()
	MON_LIST.clear()

	for thr in mons:
		thr.join()
	mons.clear()

if __name__ == '__main__':
	exit(main())
