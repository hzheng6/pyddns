import ipaddress

from typing import Union

class IpGetters:

	def GetCurrentIp(self) -> Union[ipaddress.IPv4Address, ipaddress.IPv6Address]:
		raise NotImplementedError('The GetCurrentIp method is not implemented')
