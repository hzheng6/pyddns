import os
import sys
import importlib.util

def _ImportProviderModule(moduleName: str):
	modulePath = os.path.join(os.path.split(__file__)[0], '{}.py'.format(moduleName))
	if not os.path.isfile(modulePath):
		raise ValueError('The python module for the given DNS provider is not found.')

	spec = importlib.util.spec_from_file_location(moduleName, modulePath)
	prvModule = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(prvModule)
	sys.modules[moduleName] = prvModule

def GetIpGetterCls(getterName: str):
	if getterName not in sys.modules:
		_ImportProviderModule(getterName)

	return getattr(sys.modules[getterName], getterName)
