import os
import sys
import importlib
import ipaddress

import netifaces # python3 -m pip install netifaces


_spec = importlib.util.spec_from_file_location('IpGetters', os.path.join(os.path.split(__file__)[0], 'IpGetters.py'))
IpGetters = importlib.util.module_from_spec(_spec)
_spec.loader.exec_module(IpGetters)
sys.modules['IpGetters'] = IpGetters

class FromInterface(IpGetters.IpGetters):

	def __init__(self, name, idx, ipVer):
		super().__init__()

		self.name = name
		self.idx = idx
		self.ipVer = ipVer

		self.niIpVer = None
		if self.ipVer == 4:
			self.niIpVer = netifaces.AF_INET
		elif self.ipVer == 6:
			self.niIpVer = netifaces.AF_INET6
		else:
			raise ValueError('The given IP version {} is not supported.'.format(self.ipVer))

	def GetCurrentIp(self):

		itfInfo = netifaces.ifaddresses(self.name)[self.niIpVer]
		return ipaddress.ip_address(itfInfo[self.idx]['addr'])
