import time
import logging
import threading

import DnsProviders
import IpGetters

class Monitor(object):

	def __init__(self, interval, src, dnsPrvs):
		super().__init__()

		self.interval = interval
		self.src = IpGetters.GetIpGetterCls(src['type'])(**src['config'])

		self.dnsProviders = []
		for prvSpec in dnsPrvs:
			self.dnsProviders.append(
				DnsProviders.GetDnsProviderCls(prvSpec['type'])(**prvSpec['config'])
			)

		self.currIp = None
		self.lastCheckTime = 0
		self.shutdownEvent = threading.Event()

		self.logger = logging.getLogger('Monitor')

	def CheckForUpdate(self):

		newIp = self.src.GetCurrentIp()

		if newIp != self.currIp:
			# Update is needed
			self.logger.info('Updating IP {}'.format(newIp))

			hasError = False
			for prv in self.dnsProviders:
				try:
					prv.UpdateRecord(newIp)
				except Exception as e:
					self.logger.exception(e)
					hasError = True

			if not hasError:
				self.currIp = newIp
			else:
				self.logger.error('Error Occurred; will try again next time')

	def Tick(self):

		currTime = time.time()
		if (currTime - self.lastCheckTime) > self.interval:
			self.CheckForUpdate()

		self.lastCheckTime = currTime

	def ServeForever(self, tickInterval = 0.5):

		while not self.shutdownEvent.is_set():
			try:
				self.Tick()
			except Exception as e:
				self.logger.exception(e)
				self.logger.error('Error Occurred; will try again next time')

			self.shutdownEvent.wait(tickInterval)

	def Shutdown(self):
		self.shutdownEvent.set()
