import ssl
import http.client
import logging
import ipaddress
import os
import sys
import importlib
import base64

from typing import Union

_spec = importlib.util.spec_from_file_location('DnsProviders', os.path.join(os.path.split(__file__)[0], 'DnsProviders.py'))
DnsProviders = importlib.util.module_from_spec(_spec)
_spec.loader.exec_module(DnsProviders)
sys.modules['DnsProviders'] = DnsProviders

API_HOST_ADDR = 'domains.google.com'

def _GetConnection():

	sslCtx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
	sslCtx.verify_mode = ssl.CERT_REQUIRED
	sslCtx.check_hostname = True
	sslCtx.minimum_version = ssl.TLSVersion.TLSv1_2

	conn = http.client.HTTPSConnection(API_HOST_ADDR, context=sslCtx)

	return conn

class gDomain(DnsProviders.DnsProviders):

	def __init__(self, domain, recName, apiUsername, apiKey):
		super().__init__()

		self.domain = domain
		self.recName = recName
		self.apiUsername = apiUsername
		self.apiKey = apiKey

		self.logger = logging.getLogger('DnsProviers::gDomain::{}.{}'.format(self.recName, self.domain))

	def _GetUri(self, ipStr: str):
		hostname = None
		if len(self.recName) > 0:
			hostname = '{}.{}'.format(self.recName, self.domain)
		else:
			hostname = self.domain

		uri = '/nic/update?hostname={}&myip={}'.format(hostname, ipStr)
		return uri

	def _GenAuthHeader(self):

		authPair = '{}:{}'.format(self.apiUsername, self.apiKey)
		return {
			'Authorization':
			'Basic {}'.format(base64.b64encode(authPair.encode()).decode())
		}

	def UpdateRecord(self,
		ip: Union[ipaddress.IPv4Address, ipaddress.IPv6Address],
	):

		uri = self._GetUri(str(ip))
		authHeader = self._GenAuthHeader()

		conn = _GetConnection()

		conn.request('POST', uri, headers=authHeader)

		resp = conn.getresponse()

		if resp.status != 200:
			raise RuntimeError('Server Resp {}'.format(resp.status))

		respBody = resp.read().decode()
		if (not respBody.startswith('good')) and (not respBody.startswith('nochg')):
			raise RuntimeError('Server Resp Error Message: {}'.format(respBody))

		self.logger.info('Updated new IP address {}'.format(ip))
