import ipaddress
from typing import Union

class DnsProviders(object):

	def UpdateRecord(self,
		ip: Union[ipaddress.IPv4Address, ipaddress.IPv6Address],
	):
		raise NotImplementedError('The UpdateRecord method is not implemented')
