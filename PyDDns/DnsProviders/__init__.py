import os
import sys
import importlib.util

def _ImportProviderModule(providerName: str):
	modulePath = os.path.join(os.path.split(__file__)[0], '{}.py'.format(providerName))
	if not os.path.isfile(modulePath):
		raise ValueError('The python module for the given DNS provider is not found.')

	spec = importlib.util.spec_from_file_location(providerName, modulePath)
	prvModule = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(prvModule)
	sys.modules[providerName] = prvModule

def GetDnsProviderCls(providerName: str):
	if providerName not in sys.modules:
		_ImportProviderModule(providerName)

	return getattr(sys.modules[providerName], providerName)
