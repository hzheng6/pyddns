import ssl
import http.client
import hashlib
import logging
import ipaddress
import random
import urllib
import time
import json
import os
import sys
import importlib

from typing import Union

_spec = importlib.util.spec_from_file_location('DnsProviders', os.path.join(os.path.split(__file__)[0], 'DnsProviders.py'))
DnsProviders = importlib.util.module_from_spec(_spec)
_spec.loader.exec_module(DnsProviders)
sys.modules['DnsProviders'] = DnsProviders

API_HOST_ADDR = 'api.nearlyfreespeech.net'

_SALT_ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
_SALT_LENGTH = 16

def _GenSalt():
	return ''.join([random.choice(_SALT_ALPHABET) for i in range(_SALT_LENGTH)])

def _GenTimeStamp():
	return str(int(time.time()))

def _GetConnection():

	sslCtx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
	sslCtx.verify_mode = ssl.CERT_REQUIRED
	sslCtx.check_hostname = True
	sslCtx.minimum_version = ssl.TLSVersion.TLSv1_2

	conn = http.client.HTTPSConnection(API_HOST_ADDR, context=sslCtx)

	return conn

def _CalcBodyHash(encBody: dict):
	return hashlib.sha1(encBody).hexdigest()

class NFSN(DnsProviders.DnsProviders):

	def __init__(self, domain, recName, ttl, apiUsername, apiKey):
		super().__init__()

		self.domain = domain
		self.recName = recName
		self.ttl = ttl
		self.apiUsername = apiUsername
		self.apiKey = apiKey

		self.logger = logging.getLogger('DnsProviers::NFSN::{}.{}'.format(self.recName, self.domain))

	def _GetUri(self, func: str):
		return '/dns/' + self.domain + '/' + func

	def _GenAuthHeader(self,
		uri: str,
		bodyHash: str
	):
		salt = _GenSalt()
		timestamp = _GenTimeStamp()

		#Calc hash: username;timestamp;salt;apiKey;uri;BodyHash
		preHashStr = ';'.join([self.apiUsername, timestamp, salt, self.apiKey, uri, bodyHash])
		authHash = hashlib.sha1(preHashStr.encode('utf-8')).hexdigest()

		authHeader = ';'.join([self.apiUsername, timestamp, salt, authHash])

		return {'X-NFSN-Authentication': authHeader}

	def _SendRequest(self,
		uri: str,
		body: dict,
	) -> Union[bytes, None]:

		encodedBody = urllib.parse.urlencode(body).encode('utf-8')
		bodyHash = _CalcBodyHash(encodedBody)

		authHeader = self._GenAuthHeader(uri, bodyHash)
		headers = {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': len(encodedBody),
			**authHeader
		}

		conn = _GetConnection()

		conn.request('POST', uri, body=encodedBody, headers=headers)

		resp = conn.getresponse()

		respLenStr = resp.getheader('Content-Length')

		if resp.status != 200:
			raise RuntimeError('Server Resp {}'.format(resp.status))

		if respLenStr is None:
			return None
		else:
			respLen = int(respLenStr)

			return resp.read(respLen)

	def _GetCurrentRec(self, type: str):

		uri = self._GetUri('listRRs')
		body = {
			'name': self.recName,
			'type': type
		}

		resJson = json.loads(self._SendRequest(uri, body))

		for rec in resJson:
			if rec['name'] == self.recName:
				return rec['data']

		return None

	def _RemoveCurrentRec(self, type: str, currData: str):

		uri = self._GetUri('removeRR')
		body = {
			'name': self.recName,
			'type': type,
			'data': currData
		}

		self._SendRequest(uri, body)
		self.logger.info('Removed old record with data {}'.format(currData))

	def _AddCurrentRec(self, type: str, data: str):

		uri = self._GetUri('addRR')
		body = {
			'name': self.recName,
			'type': type,
			'data': data,
			'ttl': self.ttl
		}

		self._SendRequest(uri, body)
		self.logger.info('Added new record with data {}'.format(data))

	def UpdateRecord(self,
		ip: Union[ipaddress.IPv4Address, ipaddress.IPv6Address],
	):
		type = None
		if ip.version == 4:
			type = 'A'
		elif ip.version == 6:
			type = 'AAAA'
		else:
			raise ValueError('IP version of {} is not supported.'.format(ip.version))

		ipStr = str(ip)

		currData = self._GetCurrentRec(type)
		if currData is not None:
			if currData == ipStr:
				return
			else:
				self._RemoveCurrentRec(type, currData)

		self._AddCurrentRec(type, ipStr)
