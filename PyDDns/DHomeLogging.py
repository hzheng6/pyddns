import os
import sys
import logging
import logging.handlers

from typing import Union

DEFAULT_FORMATTER = logging.Formatter('[%(asctime)s](%(levelname)s)\t<%(name)s>\t%(message)s')

STDOUT_HANDLER = logging.StreamHandler(sys.stdout)
STDERR_HANDLER = logging.StreamHandler(sys.stderr)

ROTATE_FILE_DEFAULT_MAXSIZE = 10 * 1024 * 1024    # 10 MB
ROTATE_FILE_DEFAULT_BK_COUNT = 5                  # 5 files

FILE_HANDLER_MAP = {}

def _AdjustRootLevel(level: int):

	if logging.root.level > level:
		logging.root.setLevel(level)

def AddStdoutOutput(fmt: logging.Formatter=DEFAULT_FORMATTER, level: int=logging.INFO):

	STDOUT_HANDLER.setFormatter(fmt)
	STDOUT_HANDLER.setLevel(level)
	_AdjustRootLevel(level)

	logging.root.addHandler(STDOUT_HANDLER)

def RemoveStdoutOutput():

	logging.root.removeHandler(STDOUT_HANDLER)

def AddStderrOutput(fmt: logging.Formatter=DEFAULT_FORMATTER, level: int=logging.INFO):

	STDERR_HANDLER.setFormatter(fmt)
	STDERR_HANDLER.setLevel(level)
	_AdjustRootLevel(level)

	logging.root.addHandler(STDERR_HANDLER)

def RemoveStderrOutput():

	logging.root.removeHandler(STDERR_HANDLER)

def AddFileHandler(filePath: Union[str, os.PathLike],
	hdl: logging.FileHandler,
	fmt: logging.Formatter=DEFAULT_FORMATTER,
	level: int=logging.INFO
):

	if filePath in FILE_HANDLER_MAP:
		raise KeyError('Log file with path {} is already in the handler set'.format(filePath))

	hdl.setFormatter(fmt)
	hdl.setLevel(level)
	_AdjustRootLevel(level)

	FILE_HANDLER_MAP[filePath] = hdl

	logging.root.addHandler(hdl)

def RemoveFileOutput(filePath: Union[str, os.PathLike]):

	hdl = FILE_HANDLER_MAP.pop(filePath, None)
	if hdl is not None:
		logging.root.removeHandler(hdl)

def AddRotateFileOutput(filePath: Union[str, os.PathLike],
	maxBytes: int=ROTATE_FILE_DEFAULT_MAXSIZE,
	backupCount: int=ROTATE_FILE_DEFAULT_BK_COUNT,
	fmt: logging.Formatter=DEFAULT_FORMATTER,
	level: int=logging.INFO
):

	hdl = logging.handlers.RotatingFileHandler(filename=filePath, mode='a',
		maxBytes=maxBytes, backupCount=backupCount)

	AddFileHandler(filePath=filePath, hdl=hdl, fmt=fmt, level=level)


def AddRegularFileOutput(filePath: Union[str, os.PathLike], mode='a',
	fmt: logging.Formatter=DEFAULT_FORMATTER,
	level: int=logging.INFO
):

	hdl = logging.FileHandler(filename=filePath, mode='a')

	AddFileHandler(filePath=filePath, hdl=hdl, fmt=fmt, level=level)
