import signal
import logging
import threading

TERMINATION_EVENT = threading.Event()

def ProcTerminateSignal(signalNum, frame):
	logging.getLogger('PyTerminateHandler::ProcTerminateSignal').info(
		'Terminate/Interrupt signal caught - setting up terminate event'
	)
	TERMINATION_EVENT.set()

def Init():
	logging.getLogger('PyTerminateHandler::Init').info(
		'Registering handler functions'
	)
	signal.signal(signal.SIGTERM, ProcTerminateSignal)
	signal.signal(signal.SIGINT , ProcTerminateSignal)
